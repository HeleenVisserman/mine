package ss;

import BordPlanning.*;

public class ToStringBoard {

// Fields
	// 3DConnect4
	// 3 constants for the Dimensions
	// These shouldn't be constants as they can change
	public static int dimX = 0;
	public static int dimY = 0;
	public static int dimZ = 0;// Z axis(-1 == inf)
	public static int win = 0;
	
	//saves the last location a mark was set to.
	public int r, h, c;

	private static final String FRST = "|";
	private static final String DELIM = "    ";
	private static final String LINE = "-----";
	private static String linebreak;

	public String[] Numbering;// Numbering the fields.
//		public String numbering;
	private String Line = Numbering[1];

	// we make fields a three dimensional array in which we can easily store the
	// board.
	private Mark[][][] fields;

	
	public ToStringBoard() {
		// TODO Auto-generated constructor stub
	}
	
	public String BoardtoString() {
		
		
		return null;
	}
	public String toString() {
		String s = "";
			
		for (int x = 0; x < dimX; x++) {
			if (x == 0) {
				s = FRST + s;
			}
			linebreak = "+";
			String row = "";
			for (int y = 0; y < dimY; y++) {
				if (Board.index(dimX - 1, dimY - 1) < 10) {
					row = row + "  " + Board.index(x, y) + "  " + "|";
				} else if (Board.index(dimX - 1, dimY - 1) < 100) {
					row = row + " " + Board.index(x, y) + "  " + "|";
				} else {
					row = row + " " + Board.index(x, y) + " " + "|";
				}
				Numbering[2 * x] = row + "\n";
				String breakline = linebreak;
				for (int j = 0; j < dimY; j++) {
					breakline = breakline + LINE + linebreak;
				}
				Numbering[2 * x + 1] = breakline + "\n";						
			}
		}
//			int var;
//			int sum;
//			if (dimX > dimY) {
//				sum = dimX - dimY;
//			} else {
//				sum = dimY - dimX;
//			}
//			if (dimX == dimY) {
//				var = dimX;
//			} else if (dimX > dimY) {
//				var = dimX - sum;
//			} else {
//				var = dimX + sum;
//			}
//			for (int y = 0; y < var; y++) {
//				linebreak = linebreak + LINE + "+";
//			}
//			if (x == 0) {
//				s = linebreak + "\n" + s + "\n" + linebreak;
//			} else {
//				s = s + "\n" + linebreak;
//			}
//			if (!(x == dimX - 1)) {
//				s = s + "\n" + FRST;
//			}
//		}
		String board = "";
	for (int i = 0; i < Numbering.length; i++) {
		board = board + Numbering[i];
	}
	System.out.println(board);
	return board;
	
	}
	
	public static void main(String[] args) {
		Board bord = new Board();
		bord.toString();
	}

}
