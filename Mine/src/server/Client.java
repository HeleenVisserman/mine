package server;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Client {

	String name;
	static int port;
	static String ipaddress;
	static Socket sock;
	
	static boolean AI;
	static final int FEATURES = 0;
	
	public enum Level { easy, medium, hard, human }
	public static Level level;
	
	final Lock lock = new ReentrantLock();
	final Condition noInOut = lock.newCondition();
	
	ServerHandler sh;
	ClientTUI clientTUI;

	
	public static void main(String[] args) {
		new Client();
	}
	
	public Client() {
		sock = null; 
		
		clientTUI = new ClientTUI();
		
		setUp();
		
		setUpSocket();
		
		sh.send(name, "HELLO " + name + " " + AI + " " + FEATURES);
		
		chatBasic();
		
	}

	public void setUp() {
		name = clientTUI.getName();
		level = clientTUI.getLevel();
		AI = (!level.equals(Level.human));
		ipaddress = clientTUI.getIP();
		port = clientTUI.getPort();
		
	}
	
	public void setUpSocket() {
		try {
			sock = new Socket(ipaddress, port);
		} catch (UnknownHostException e) {
			System.out.println("could not find host");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		sh = new ServerHandler(sock, this);
		sh.start(); // hij zit op een aparte thread anders blokkeert hij het hele programma
		
		while (!sh.run) {
			System.out.println("waiting for initialization");
//			lock.lock();
//			try {
//				noInOut.await();
//				System.out.println("waiting for initialization");
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} finally {
//				lock.unlock();
//				
//			}
		}
		
		
	}
	
	public void chatBasic() {
		Scanner scan = new Scanner(System.in);
		
		while (true) {
			sh.send(name, scan.nextLine());
			
		}
	}
	
	public void displayMenu() {
		System.out.println("Welcome to the menu");
		System.out.println("1 - request a game");
		System.out.println("0 - Quit");
		System.out.println("");
	}
}
