package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ServerHandler extends Thread {

	Socket sock;
	BufferedReader in;
	PrintWriter out;
	boolean run;
	Client client;
	
	Lock lock = new ReentrantLock();
	
	public ServerHandler(Socket sock, Client client) {
		this.sock = sock;
		this.client = client;
	}
	
	
	
	@Override
	public void run() {
		initialize();
		
		inputLoop();
		
		close();
	}

	private void close() {
		// TODO Auto-generated method stub
		
	}

	private void inputLoop() {
		String line;
		try {
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				if (line.startsWith("WELCOME")) {
					String[] parts = line.split(" ");
					
					System.out.println("ID: " + parts[1]);
				}
			}
		} catch (IOException e1) {
			System.out.println("Server disconnected");
		}
		
//		while(run) {
//			try {
//				String line = in.readLine();
//				
//				System.out.println(line);
//				
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				break;
//			}
//		}
//		
	}

	private synchronized void initialize() {
//		lock.lock();
		try {
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(sock.getOutputStream(), true); //the true creates an autoflush
			run = true;
			System.out.println("run is true");
		} catch (IOException e) {
			System.out.println("initializing failed");
			run = false;
			e.printStackTrace();
//		} finally {
//			lock.unlock();
		}
		
	}
	
	public void send(String name, String msg) {
		out.println(name + ": " + msg);
	}
	
	
	

}
