package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientHandler extends Thread {

	Socket sock;
	BufferedReader in;
	PrintWriter out;
	boolean run;
	Server server;
	static int count = 0;
	
	String clientName;
	String ID;
	String features;
	String AI;
	
	
	public ClientHandler(Socket sock, Server server) {
		
		this.sock = sock;
		
		this.server = server;
		
	}
	
	@Override
	public void run() {
		initialize();
		
		inputLoop();
		
		close();
	}

	private void inputLoop() {
		String line;
		try {
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				Scanner lineScanner = new Scanner(line);
				lineScanner.next();
				String line2 = lineScanner.next();
				if (line2.startsWith("HELLO")) {
					String[] parts = line2.split(" ");
					parts[1] = clientName;
					receiveHELLO();
				}
			}
		} catch (IOException e1) {
			System.out.println("client" + clientName + "disconnected");
		} finally {
			this.close();
		}
		
	}

	private void initialize() {
		try {
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(sock.getOutputStream(), true); //the true creates an autoflush
			run = true;
		} catch (IOException e) {
			System.out.println("initializing failed");
			run = false;
			e.printStackTrace();
		}
		
	}
	
	public void send(String name, String msg) {
		out.println(name + ": " + msg);
	}
	
	public void receiveHELLO() {
		//TODO: add timeout
		out.println("WELCOME " + count + server.timeout + server.FEATURES);
		count++;
	}
	
	public void close() {
		try {
			sock.close();
			out.close();
			in.close();
			
		} catch (IOException e) {
			System.out.println("Error while closing clienthandler");
			e.printStackTrace();
		}
		
	}
	
	

}
