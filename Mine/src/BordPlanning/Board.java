package BordPlanning;

import java.util.Scanner;

import BordPlanning.Mark;
import ss.Board;

public class Board {

	// Fields
	// 3DConnect4
	// 3 constants for the Dimensions
	// These shouldn't be constants as they can change
	public static int dimX = 0;
	public static int dimY = 0;
	public static int dimZ = 0;// Z axis(-1 == inf)
	public static int win = 0;

	private static final String FRST = "|";
	private static final String DELIM = "    ";
	private static final String LINE = "-----";
	private static String linebreak;

	public String[] Numbering;// Numbering the fields.
	private String Line = Numbering[1];

	// we make fields a three dimensional array in which we can easily store the
	// board.
	private Mark[][][] fields;

	// Constructors
	/*
	 * @ ensures (\forall int i ; 0 <= i && i < DIMX*DIMY; this.field[i] ==
	 * MARK.EMPTY;
	 */
	public Board(int diX, int diY, int diZ, int winz) {
		dimX = diX;
		dimY = diY;
		dimZ = diZ;
		win = winz;

		if (dimZ == -1) {
			fields = new Mark[dimX][dimY][Integer.MAX_VALUE];
		} else {
			fields = new Mark[dimX][dimY][dimZ];
		}
		for (int x = 0; x < dimX; x++) {
			for (int y = 0; y < dimY; y++) {
				for (int z = 0; z < dimZ; z++) {
					fields[x][y][z] = Mark.EMPTY;
				}
			}
		}

	}

	public Board() {
		this(4, 4, 4, 4);
	}

	// ---------------------- Methods
	// -------------------------------------------------------

	public Board deepCopy() {
		// We will have to change the option to create a copy of a bigger board
		Board board = new Board(this.dimX, this.dimY, this.dimZ);
		for (int x = 0; x < dimX; x++) {
			for (int y = 0; y < dimY; y++) {
				for (int z = 0; z < dimZ; z++) {
					board.setField(x, y, z, fields[x][y][z]);
				}
			}
		}
		return board;
	}

	/*
	 * @ requires int row; 0 <= row && row < DIMX; requires int col; 0 <= col &&
	 * col < DIMY;
	 */
	// I think we should add a z axis counting the number of the board(height)
	public int index(int x, int y) {
		return x * dimY + y;
	}

	/*
	 * converts an index into an x coordinate
	 */
	public int iToX(int i) {
		return (i - (i % dimY)) / dimY;
	}

	/*
	 * converts an index into a Y coordinate
	 */
	public int iToY(int i) {
		return i % dimY;
	}

	// if height equals -1 (inf) than it should not check for height (with an if
	// statement that checks
	// whether z = -1.
	/*
	 * @ requires int index; ensures \result == (0 <= index && index < DIMX *
	 * DIMY * DIMZ);
	 */
	public boolean isField(int index) {
		return 0 <= index && index < dimX * dimY;
	}

	/*
	 * @ requires int row;int col; ensures \result ==( 0 <= row && row < DIMX *
	 * DIMY * DIMZ && 0 <= col && col < DIMX * DIMY * DIMZ );
	 */
	public boolean isField(int x, int y) {
		return 0 <= x && x < dimX && 0 <= y && y <= dimY;
	}

	/*
	 * @ requires this.isField(i); ensures \result == MARK.XX || \result ==
	 * MARK.OO || \result == MARK.EMPTY ;
	 */
	public Mark getField(int i, int z) {
		return fields[iToX(i)][iToY(i)][z];
	}

	/*
	 * @ requires this.isField(row, col); ensures \ result == MARK.XX || \result
	 * == MARK.OO || \result == MARK.EMPTY
	 */
	public Mark getField(int x, int y, int z) {
		return fields[x][y][z];
	}

	/*
	 * @ requires this.isField(i) ; ensures \result == (this.getField(i)==
	 * MARK.EMPTY );
	 */
	public boolean hasEmptyField(int i) {
		return hasEmptyField(iToX(i), iToY(i));
	}

	// Make a for loop about the z axis .
	// If the mark equals -1 then the field is empty
	/*
	 * @ requires this.isField(row, col) ensures \result (this.getField(row,
	 * col) == MARK.EMPTY);
	 */
	public boolean hasEmptyField(int x, int y) {
		if (dimZ == -1) {
			return true;
		} else {
			for (int z = 0; z < dimZ; z++) {
				if (getField(x, y, z) == Mark.EMPTY) {
					return true;
				}
			}
		}
		return false;
	}

	// Methods should check whether all boards are complete.
	// Not check if z-axis equals -1.
	/*
	 * @ requires (\forall int i; 0 <= i & i < DIMX * DIMY * DIMZ &&
	 * this.getField(i)) != MARK.EMPTY;)
	 */
	public boolean isFull() {
		if (dimZ == -1) {
			return false;
		} else {
			for (int i = 0; i < dimX * dimY; i++) {
				for (int z = 0; z < dimZ; z++) {
					if (getField(i, z) == Mark.EMPTY) {
						return false;
					}
				}
			}
		}
		return true;
	}

	// when board is full or we have a winner.
	/*
	 * @ requires this.isFull(); ensures \result == hasWinner() || \result ==
	 * isFull()
	 */
	public boolean gameOver() {
		return this.isFull() || this.hasWinner();
	}

	// Check every row in every board.
	/*
	 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
	 * this.hasWinner())
	 */
	
	// checks for the last move (when inputted) whether this move made a "win" in a row.
	public boolean hasWinY(int x, int y, int z, Mark m) {
		int county = 0;
		
		int yplus = y;
		int ymin = y;
		ymin--;
		
		while (getField(x, yplus, z) == m && y < dimY) {
			county++;
			yplus++;
			
		}
		while (getField(x, ymin, z) == m && y >= 0) {
			county++;
			ymin--;
		}
		if (county >= win) {
			return true;
		}
		
		
		
	}
	
	
	
	
	
	
	public boolean hasX(Mark m) {
		for (int z = 0; z < dimZ; z++) {
			for (int x = 0; x < dimX; x++) {
				Boolean hasX = true;
				for (int y = 0; y < dimY; y++) {
					if (getField(x, y, z) != m) {
						hasX = false;
					}
				}
				if (hasX) {
					return true;
				}
			}
		}
		return false;
	}

	// Check every columns.
	/*
	 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
	 * this.hasWinner())
	 */
	public boolean hasY(Mark m) {
		for (int z = 0; z < dimZ; z++) {
			for (int y = 0; y < dimY; y++) {
				Boolean hasY = true;
				for (int x = 0; x < dimX; x++) {
					if (getField(x, y, z) != m) {
						hasY = false;
					}
				}
				if (hasY) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean hasZ(Mark m) {
		for (int y = 0; y < dimY; y++) {
			for (int x = 0; x < dimX; x++) {
				Boolean hasZ = true;
				for (int z = 0; z < dimZ; z++) {
					if (getField(x, y, z) != m) {
						hasZ = false;
					}
				}
				if (hasZ) {
					return true;
				}
			}
		}
		return false;
	}

	// If stat. if the Dimx equals Dimy.
	/*
	 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
	 * this.hasWinner())
	 */
	// If stat. if the Dimx equals Dimy.
	public boolean hasDiagonalHoriz(Mark m) {
		if (dimX != dimY) {
			return false;
		}

		Boolean hasDiagonal1 = true;
		Boolean hasDiagonal2 = true;

		for (int z = 0; z < dimZ; z++) {
			for (int i = 0; i < dimX && i < dimY; i++) {
				if (getField(i, i, z) != m) {
					hasDiagonal1 = false;
				}
				if (getField(dimX - 1 - i, i, z) != m) {
					hasDiagonal2 = false;
				}
			}
			if (hasDiagonal1 || hasDiagonal2) {
				return true;
			}
		}

		return false;
	}

	// Checking diagonal in Z-axis-vertical
	/*
	 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
	 * this.hasWinner())
	 */
	public boolean hasDiagonalVertiX(Mark m) {
		if (dimX != dimZ) {
			return false;
		}

		Boolean hasDiagonal1 = true;
		Boolean hasDiagonal2 = true;

		for (int y = 0; y < dimY; y++) {
			for (int i = 0; i < dimX && i < dimZ; i++) {
				if (getField(i, y, i) != m) {
					hasDiagonal1 = false;
				}
				if (getField(dimX - 1 - i, y, i) != m) {
					hasDiagonal2 = false;
				}
			}
			if (hasDiagonal1 || hasDiagonal2) {
				return true;
			}
		}

		return false;
	}

	public boolean hasDiagonalVertiY(Mark m) {
		if (dimY != dimZ) {
			return false;
		}

		Boolean hasDiagonal1 = true;
		Boolean hasDiagonal2 = true;

		for (int x = 0; x < dimX; x++) {
			for (int i = 0; i < dimY && i < dimZ; i++) {
				if (getField(x, i, i) != m) {
					hasDiagonal1 = false;
				}
				if (getField(x, dimY - 1 - i, i) != m) {
					hasDiagonal2 = false;
				}
			}
			if (hasDiagonal1 || hasDiagonal2) {
				return true;
			}
		}

		return false;
	}

	// Checking diagonal in z-axis-Overall
	/*
	 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
	 * this.hasWinner())
	 */
	public boolean hasDiagonalOver(Mark m) {
		if (!(dimX == dimY && dimX == dimZ && dimZ == dimY)) {
			return false;
		}
		boolean hasDiagonal1 = true;
		boolean hasDiagonal2 = true;
		boolean hasDiagonal3 = true;
		boolean hasDiagonal4 = true;

		for (int i = 0; i < dimY && i < dimX && i < dimZ; i++) {
			if (getField(i, i, i) != m) {
				hasDiagonal1 = false;
			}
			if (getField(i, i, dimZ - 1 - i) != m) {
				hasDiagonal2 = false;
			}
			if (getField(i, dimY - 1 - i, i) != m) {
				hasDiagonal3 = false;
			}
			if (getField(dimX - 1 - i, i, i) != m) {
				hasDiagonal4 = false;
			}
			if (hasDiagonal1 || hasDiagonal2 || hasDiagonal3 || hasDiagonal4) {
				return true;
			}
		}
		return false;

	}

	// Must have either(rows||columns||diagonalHor||diagonalVert||diagonalOver).
	/*
	 * @ ensures (\result == hasRow(m) || \result == hasColumn(m) || \result ==
	 * hasdiagonalHor(m) || \result == hasdiagonalVert(m) ||\result ==
	 * hasdiagonalOver(m))
	 */
	public boolean isWinner(Mark m) {
		return hasX(m) || hasY(m) || hasZ(m) || hasDiagonalHoriz(m) || hasDiagonalVertiX(m) || hasDiagonalVertiY(m)
				|| hasDiagonalOver(m);
	}

	// declare the winner XX||OO
	/*
	 * @ ensures (\result == isWinner(MARK.XX) || \result == isWinner(MARK.OO))
	 */
	public boolean hasWinner() {
		return isWinner(Mark.XX) || isWinner(Mark.OO);
	}

	// Prints the numbering.
	public String numbering() {
		String s = "";
		for (int x = 0; x < dimX; x++) {
			if (x == 0) {
				s = FRST + s;
			}
			linebreak = "+";
			for (int y = 0; y < dimY; y++) {
				if (index(dimX - 1, dimY - 1) < 10) {
					s = s + "  " + index(x, y) + "  " + "|";
				} else if (index(dimX - 1, dimY - 1) < 100) {
					s = s + " " + index(x, y) + "  " + "|";
				} else {
					s = s + " " + index(x, y) + " " + "|";
				}
			}
			int var;
			int sum;
			if (dimX > dimY) {
				sum = dimX - dimY;
			} else {
				sum = dimY - dimX;
			}
			if (dimX == dimY) {
				var = dimX;
			} else if (dimX > dimY) {
				var = dimX - sum;
			} else {
				var = dimX + sum;
			}
			for (int y = 0; y < var; y++) {
				linebreak = linebreak + LINE + "+";
			}
			if (x == 0) {
				s = linebreak + "\n" + s + "\n" + linebreak;
			} else {
				s = s + "\n" + linebreak;
			}
			if (!(x == dimX - 1)) {
				s = s + "\n" + FRST;
			}
		}
		numbering = s;
	}

	public String zaxis() {
		String s = "";
		for (int z = 0; z < dimZ; z++) {
			s = s + toString() + "\n" + "\n";
		}
		return s;
	}

	// reset all the boards.
	/*
	 * @ ensures(\forall int i; 0 <= i && i< DIMX * DIMY * DIMZ)
	 */
	public void reset() {
		for (int x = 0; x < dimX; x++) {
			for (int y = 0; y < dimY; y++) {
				for (int z = 0; z < dimZ; z++) {
					setField(x, y, z, Mark.EMPTY);
				}
			}
		}
	}

	// set fields for the players(Automatically goes to the next Board if a
	// field is already full)
	/*
	 * @ requires this.isField(i) ensures \result ( this.getField() == m);
	 */
	public void setField(int i, Mark m) {
		setField(iToX(i), iToY(i), m);
	}

	// set fields for the players(Automatically goes to the next Board if a
	// field is already full)
	/*
	 * @ requires this.isField(x, y); ensures \result (this.getField(x, y) ==
	 * m);
	 */
	public void setField(int x, int y,   Mark m) {
		if (dimZ == -1) {
			int z = 0;
			while (true) {
				if (getField(x, y, z) == Mark.EMPTY) {
					fields[x][y][z] = m;
					break;
				}
				z++;
			}
			
//fdklfdk
		} else {
			for (int z = 0; z < dimZ; z++) {
				if (getField(x, y, z) == Mark.EMPTY) {
					fields[x][y][z] = m;
					break;
				}
			}
		}
	}

	// set fields for the players(Automatically goes to the next Board if a
	// field is already full)
	public void setField(int x, int y, int z, Mark m) {
		fields[x][y][z] = m;
	}

}