package BordPlanning;

import BordPlanning.BoardCopy;

public class WinChecks {

	package BordPlanning;

	import BordPlanning.Mark;

	import java.util.Scanner;

	//import BordPlanning.Mark;

	public class Board {

		// Fields
		// 3DConnect4
		// 3 constants for the Dimensions
		// These shouldn't be constants as they can change
		public static int dimX = 0;
		public static int dimY = 0;
		public static int dimZ = 0;// Z axis(-1 == inf)
		public static int win = 0;
		
		//saves the last location a mark was set to.
		public int r, h, c;

		private static final String FRST = "|";
		private static final String DELIM = "    ";
		private static final String LINE = "-----";
		private static String linebreak;

//		public String[] Numbering;// Numbering the fields.
		public String numbering;
//		private String Line = Numbering[1];

		// we make fields a three dimensional array in which we can easily store the
		// board.
		private Mark[][][] fields;

		// Constructors
		/*
		 * @ ensures (\forall int i ; 0 <= i && i < DIMX*DIMY; this.field[i] ==
		 * MARK.EMPTY;
		 */
		public Board(int diX, int diY, int diZ, int winz) {
			dimX = diX;
			dimY = diY;
			dimZ = diZ;
			win = winz;

			if (dimZ == -1) {
				fields = new Mark[dimX][dimY][Integer.MAX_VALUE];
			} else {
				fields = new Mark[dimX][dimY][dimZ];
			}
			for (int x = 0; x < dimX; x++) {
				for (int y = 0; y < dimY; y++) {
					for (int z = 0; z < dimZ; z++) {
						fields[x][y][z] = Mark.EMPTY;
					}
				}
			}

		}

		public Board() {
			this(4, 4, 4, 4);
		}

		// ---------------------- Methods
		// -------------------------------------------------------

		public Board deepCopy() {
			// We will have to change the option to create a copy of a bigger board
			Board board = new Board(this.dimX, this.dimY, this.dimZ, this.win);
			for (int x = 0; x < dimX; x++) {
				for (int y = 0; y < dimY; y++) {
					for (int z = 0; z < dimZ; z++) {
						board.setField(x, y, z, fields[x][y][z]);
					}
				}
			}
			return board;
		}

		/*
		 * @ requires int row; 0 <= row && row < DIMX; requires int col; 0 <= col &&
		 * col < DIMY;
		 */
		// I think we should add a z axis counting the number of the board(height)
		public int index(int x, int y) {
			return x + y * dimX;
		}

		/*
		 * converts an index into an x coordinate
		 */
		public int iToX(int i) {
			return (i % dimX);
		}

		/*
		 * converts an index into a Y coordinate
		 */
		public int iToY(int i) {
			return (i-(i%dimX));
		}

		// if height equals -1 (inf) than it should not check for height (with an if
		// statement that checks
		// whether z = -1.
		/*
		 * @ requires int index; ensures \result == (0 <= index && index < DIMX *
		 * DIMY * DIMZ);
		 */
		public boolean isField(int index) {
			return 0 <= index && index < dimX * dimY;
		}

		/*
		 * @ requires int row;int col; ensures \result ==( 0 <= row && row < DIMX *
		 * DIMY * DIMZ && 0 <= col && col < DIMX * DIMY * DIMZ );
		 */
		public boolean isField(int x, int y) {
			return 0 <= x && x < dimX && 0 <= y && y <= dimY;
		}

		/*
		 * @ requires this.isField(i); ensures \result == MARK.XX || \result ==
		 * MARK.OO || \result == MARK.EMPTY ;
		 */
		public Mark getField(int i, int z) {
			return fields[iToX(i)][iToY(i)][z];
		}

		/*
		 * @ requires this.isField(row, col); ensures \ result == MARK.XX || \result
		 * == MARK.OO || \result == MARK.EMPTY
		 */
		public Mark getField(int x, int y, int z) {
			return fields[x][y][z];
		}

		/*
		 * @ requires this.isField(i) ; ensures \result == (this.getField(i)==
		 * MARK.EMPTY );
		 */
		public boolean hasEmptyField(int i) {
			return hasEmptyField(iToX(i), iToY(i));
		}

		// Make a for loop about the z axis .
		// If the mark equals -1 then the field is empty
		/*
		 * @ requires this.isField(row, col) ensures \result (this.getField(row,
		 * col) == MARK.EMPTY);
		 */
		public boolean hasEmptyField(int x, int y) {
			if (dimZ == -1) {
				return true;
			} else {
				for (int z = 0; z < dimZ; z++) {
					if (getField(x, y, z) == Mark.EMPTY) {
						return true;
					}
				}
			}
			return false;
		}

		// Methods should check whether all boards are complete.
		// Not check if z-axis equals -1.
		/*
		 * @ requires (\forall int i; 0 <= i & i < DIMX * DIMY * DIMZ &&
		 * this.getField(i)) != MARK.EMPTY;)
		 */
		public boolean isFull() {
			if (dimZ == -1) {
				return false;
			} else {
				for (int i = 0; i < dimX * dimY; i++) {
					for (int z = 0; z < dimZ; z++) {
						if (getField(i, z) == Mark.EMPTY) {
							return false;
						}
					}
				}
			}
			System.out.println("board is full");
			return true;
		}

		// when board is full or we have a winner.
		/*
		 * @ requires this.isFull(); ensures \result == hasWinner() || \result ==
		 * isFull()
		 */
		public boolean gameOver() {
			return this.isFull() || this.hasWinner();
		}

		// Check every row in every board.
		/*
		 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
		 * this.hasWinner())
		 */
		
		// checks for the last move (when inputted) whether this move made a "win" in a row.
		public boolean hasWinY(Mark m) {
			int county = 0;
			
			int yplus = c;
			int ymin = c;
			ymin--;
			
			while (yplus < dimY && getField(r, yplus, h) == m ) {
				county++;
				yplus++;
				
			}
			while (ymin >= 0 && getField(r, ymin, h) == m) {
				county++;
				ymin--;
			}
			if (county >= win) {
				System.out.println("hasWiny win");
			}
			return county >= win;
		}
		
		public boolean hasWinX(Mark m) {
			int countx = 0;
			
			int xplus = r;
			int xmin = r;
			xmin--;
			
			while (xplus < dimX && getField(xplus, c, h) == m) {
				countx++;
				xplus++;
				
			}
			while (xmin >= 0 && getField(xmin, c, h) == m) {
				countx++;
				xmin--;
			}
			if (countx >= win) {
				System.out.println("hasWinx win");
			}
			return countx >= win;
		}
		
		public boolean hasWinZ(Mark m) {
			int countz = 0;
			
			int zplus = h;
			int zmin = h;
			zmin--;
			
			while (zplus < dimZ && getField(r, c, zplus) == m ) {
				countz++;
				zplus++;
				
			}
			while (zmin >= 0 && getField(r, c, zmin) == m) {
				countz++;
				zmin--;
			}
			if (countz >= win) {
				System.out.println("hasWinZ win");
			}
			return countz >= win;
		}

		public boolean hasDiagonalXY(Mark m) {		
			int count1 = 0;
			
			int xplus = r;
			int xmin = r;
			xmin--;
			
			int yplus = c;
			int ymin = c;
			ymin--;
			
			//for the line (i, i, h)
			while (xplus < dimX && yplus < dimY && getField(xplus, yplus, h).equals(m)) {
				count1++;
				yplus++;
				xplus++;
			}
			while (xmin >= 0 && ymin >= 0 && getField(xmin, ymin, h).equals(m)) {
				count1++;
				ymin--;
				xmin--;
			} if (count1 >= win) {
				System.out.println("has diagonal (i, i, h) (XY)");
				return true;
			}
			
			//for the line (i, dimY - i - 1, h)
			int count2 = 0;
			
			int xplus2 = r;
			int xmin2 = r;
			xmin--;
			
			int yplus2 = c;
			int ymin2 = c;
			ymin--;

			while (xplus2 < dimX && yplus2 < dimY && getField(xplus2, dimY - yplus2 - 1, h).equals(m) ) {
				count2++;
				yplus2++;
				xplus2++;
			}
			while (xmin2 >= 0 && ymin2 >= 0 && getField(xmin2, dimY - ymin2 - 1, h).equals(m)) {
				count2++;
				ymin2--;
				xmin2--;
			} if (count2 >= win) {
				System.out.println("has diagonal (i, dimY - ymin2 - 1, h) (XY)");
			}
			return count2 >= win;
		}
		
		
		// If stat. if the Dimx equals Dimy.
		/*
		 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
		 * this.hasWinner())
		 */
		// If stat. if the Dimx equals Dimy.
	//	
		
		// Checking diagonal in Z-axis-vertical
		/*
		 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
		 * this.hasWinner())
		 */
		public boolean hasDiagonalZX(Mark m) {
			int count1 = 0;
			
			int xplus = r;
			int xmin = r;
			xmin--;
			
			int zplus = h;
			int zmin = h;
			zmin--;
			
			//for the line (i, i, h)
			while (xplus < dimX && zplus < dimZ && getField(xplus, c, zplus).equals(m)) {
				count1++;
				zplus++;
				xplus++;
			}
			while (xmin >= 0 && zmin >= 0 && getField(xmin, c, zmin).equals(m)) {
				count1++;
				zmin--;
				xmin--;
			} if (count1 >= win) {
				System.out.println("has diagonal (i, c, i) (ZX)");
				return true;
			}
			
			//for the line (i, dimY - i - 1, h)
			int count2 = 0;
			
			int xplus2 = r;
			int xmin2 = r;
			xmin--;
			
			int zplus2 = h;
			int zmin2 = h;
			zmin--;

			while (xplus2 < dimX && zplus2 < dimZ && getField(xplus2, c, dimZ - zplus2 - 1).equals(m)) {
				count2++;
				zplus2++;
				xplus2++;
			}
			while (xmin2 >= 0 && zmin2 >= 0 && getField(xmin2, c, dimZ - zmin2 - 1).equals(m)) {
				count2++;
				zmin2--;
				xmin2--;
			} if (count2 >= win) {
				System.out.println("has diagonal (i, c, dimZ - zmin2 - 1) (ZX)");
			}
			return count2 >= win;
		}

		public boolean hasDiagonalZY(Mark m) {
			int count1 = 0;
			
			int yplus = c;
			int ymin = c;
			ymin--;
			
			int zplus = h;
			int zmin = h;
			zmin--;
			
			//for the line (i, i, h)
			while (yplus < dimY && zplus < dimZ && getField(r, yplus, zplus) == m) {
				count1++;
				zplus++;
				yplus++;
			}
			while (ymin >= 0 && zmin >= 0 && getField(r, ymin, zmin) == m) {
				count1++;
				zmin--;
				ymin--;
			} if (count1 >= win) {
				System.out.println("has diagonal (r, i, i) (ZY)");
				return true;
			}
			
			//for the line (i, dimY - i - 1, h)
			int count2 = 0;
			
			int yplus2 = c;
			int ymin2 = c;
			ymin--;
			
			int zplus2 = h;
			int zmin2 = h;
			zmin--;

			while (yplus2 < dimY && zplus2 < dimZ && getField(r, yplus2, dimZ - zplus2 - 1) == m) {
				count2++;
				zplus2++;
				yplus2++;
			}
			while (ymin2 >= 0 && zmin2 >= 0 && getField(r, ymin2, dimZ - zmin2 - 1) == m) {
				count2++;
				zmin2--;
				ymin2--;
			} 
			if (count2 >= win) {
				System.out.println("has diagonal (r, i, dimZ - zmin2 - 1) (ZY)");
			}
			return count2 >= win;

		}

		// Checking diagonal in z-axis-Overall
		/*
		 * @ requires (\forall int i; 0 <= i && i < DIMX * DIMY * DIMZ &&
		 * this.hasWinner())
		 */
		public boolean hasDiagonalOver(Mark m) {
			int count1 = 0;
			
			int xplus1 = r;
			int xmin1 = r;
			xmin1--;
			
			int yplus1 = c;
			int ymin1 = c;
			ymin1--;
			
			int zplus1 = c;
			int zmin1 = c;
			zmin1--;
			
			while (xplus1 < dimX && yplus1 < dimY && zplus1 < dimZ && getField(xplus1, yplus1, zplus1) == m) {
				count1++;
				xplus1++;
				yplus1++;
				zplus1++;
			}
			while (xmin1 >= 0 && ymin1 >= 0 && zmin1 >= 0 && getField(xmin1, ymin1, zmin1) == m) {
				count1++;
				xmin1--;
				ymin1--;
				zmin1--;
			}
			if (count1 >= win) {
				System.out.println("has diagonal (i,i,i)");
				return true;
			}
			
			//for (DIMx-1-i, i, i)
			int count2 = 0;
			
			int xplus2 = r;
			int xmin2 = r;
			xmin2--;
			
			int yplus2 = c;
			int ymin2 = c;
			ymin2--;
			
			int zplus2 = c;
			int zmin2 = c;
			zmin2--;
			
			while (xplus2 < dimX && yplus2 < dimY && zplus2 < dimZ && getField(dimX - 1 - xplus2, yplus2, zplus2) == m) {
				count2++;
				xplus2++;
				yplus2++;
				zplus2++;
			}
			while (xmin2 >= 0 && ymin2 >= 0 && zmin2 >= 0 && getField(dimX - 1 - xmin2, ymin2, zmin2) == m) {
				count2++;
				xmin2--;
				ymin2--;
				zmin2--;
			}
			if (count2 >= win) {
				System.out.println("has diagonal (DIMx-1-i, i, i)");
				return true;
			}
			
			//for line (i, dimY - 1 - i, i)
			int count3 = 0;
			
			int xplus3 = r;
			int xmin3 = r;
			xmin3--;
			
			int yplus3 = c;
			int ymin3 = c;
			ymin3--;
			
			int zplus3 = c;
			int zmin3 = c;
			zmin3--;
			
			while (xplus3 < dimX && yplus3 < dimY && zplus3 < dimZ && getField(xplus3, dimY - 1 - yplus3, zplus3) == m) {
				count3++;
				xplus3++;
				yplus3++;
				zplus3++;
			}
			while (xmin3 >= 0 && ymin3 >= 0 && zmin3 >= 0 && getField(xmin3, dimY - 1 - ymin3, zmin3) == m) {
				count3++;
				xmin3--;
				ymin3--;
				zmin3--;
			}
			if (count3 >= win) {
				System.out.println("winner diagonal (i, dimY - 1 - i, i)");
				return true;
			}
			
			//for line (i, i, dimZ - i - 1)
			int count4 = 0;
			
			int xplus4 = r;
			int xmin4 = r;
			xmin4--;
			
			int yplus4 = c;
			int ymin4 = c;
			ymin4--;
			
			int zplus4 = c;
			int zmin4 = c;
			zmin4--;
			
			while (xplus4 < dimX && yplus4 < dimY && zplus4 < dimZ && getField(xplus4, yplus4, dimZ - 1 - zplus4) == m) {
				count4++;
				xplus4++;
				yplus4++;
				zplus4++;
			}
			while (xmin4 >= 0 && ymin4 >= 0 && zmin4 >= 0 && getField(xmin4, ymin4, dimZ - 1 - zmin4) == m) {
				count4++;
				xmin4--;
				ymin4--;
				zmin4--;
			}
			if (count4 >= win) {
				System.out.println("winner diagonal (i, i, dimZ - i - 1)");
			}
			return count4 >= win;
			
		}

		// Must have either(rows||columns||diagonalHor||diagonalVert||diagonalOver).
		/*
		 * @ ensures (\result == hasRow(m) || \result == hasColumn(m) || \result ==
		 * hasdiagonalHor(m) || \result == hasdiagonalVert(m) ||\result ==
		 * hasdiagonalOver(m))
		 */
		/*
		 * Checks after a move whether that move  has created a winner. So it needs as an input the most recently set mark
		 * and the mark that has been set.
		 */
		public boolean isWinner(Mark m) {
			return hasWinX(m) || hasWinY(m) || hasWinZ(m) || hasDiagonalXY(m) || hasDiagonalZX(m) || hasDiagonalZY(m)
					|| hasDiagonalOver(m);
		}

		// declare the winner XX||OO
		/*
		 * @ ensures (\result == isWinner(MARK.XX) || \result == isWinner(MARK.OO))
		 */
		public boolean hasWinner() {
			if (isWinner(Mark.XX) || isWinner(Mark.OO)){
				System.out.println();
				return true;
			}
			return false;
		}

		// Prints the numbering.
		public String toString() {
			String board = "";
			
			String line = "---";
			for (int i = 0; i < dimX - 1; i++) {
				line = line + "+---";
			}
			
			for (int z = 0; z < dimZ; z++) {
				String s = "";
				for (int x = 0; x < dimX; x++) {
					String row = "";
					for (int y = 0; y < dimY; y++) {
						row = row + " " + getField(x, y, z).toString() + " ";
						if (y < dimY - 1) {
							row = row + "|";
						}
						
					}
					s = s + row;
					if (x < dimX - 1) {
						s = s + "\n" + line + "\n";
					}
					
				}
				
			//	System.out.println("Z: " + z + "\n" + s);
				board = "Z: " + z + "\n" + s + "\n" + "\n" + board;
			}
			return board + "\n" + indexesBoard();
		}
		
		
		public String indexesBoard() {
			String board = "";
			
			String line = "-----";
			for (int i = 0; i < dimX - 1; i++) {
				line = line + "+-----";
			}
			
			String s = "";
			for (int x = 0; x < dimX; x++) {
				String row = "";
				for (int y = 0; y < dimY; y++) {
					
					String part = new String();
					String space = new String();
					String space2 = new String();
					
					if (index(x, y) < 10) {
						space = space2 = "  ";
					} else if (index(x, y) < 100) {
						space = " ";
						space2 = "  ";
					} else {
						space = space2 = " ";
						
					}
					
					row = row + space + index(x, y) + space2;
					if (y < dimY - 1) {
						row = row + "|";
					}				
					
				}
				s = s + row;
				if (x < dimX - 1) {
					s = s + "\n" + line + "\n";
				}
				
			}
			//	System.out.println("Z: " + z + "\n" + s);
			board = "Indexes board:" + "\n" + s + "\n" + "\n" + board;
			return board;
		}

		// reset all the boards.
		/*
		 * @ ensures(\forall int i; 0 <= i && i< DIMX * DIMY * DIMZ)
		 */
		public void reset() {
			for (int x = 0; x < dimX; x++) {
				for (int y = 0; y < dimY; y++) {
					for (int z = 0; z < dimZ; z++) {
						setField(x, y, z, Mark.EMPTY);
					}
				}
			}
		}

		// set fields for the players(Automatically goes to the next Board if a
		// field is already full)
		/*
		 * @ requires this.isField(i) ensures \result ( this.getField() == m);
		 */
		public void setField(int i, Mark m) {
			setField(iToX(i), iToY(i), m);
		}

		// set fields for the players(Automatically goes to the next Board if a
		// field is already full)
		/*
		 * @ requires this.isField(x, y); ensures \result (this.getField(x, y) ==
		 * m);
		 */
		public void setField(int x, int y,   Mark m) {
			if (dimZ == -1) {
				int z = 0;
				while (true) {
					if (getField(x, y, z) == Mark.EMPTY) {
						fields[x][y][z] = m;
						r = y;
						c = x;
						h = z;
						break;
					}
					z++;
				}
			} else {
				for (int z = 0; z < dimZ; z++) {
					if (getField(x, y, z) == Mark.EMPTY) {
						fields[x][y][z] = m;
						r = y;
						c = x;
						h = z;
						break;
					}
				}
			}
			
		}

		// set fields for the players(Automatically goes to the next Board if a
		// field is already full)
		public void setField(int x, int y, int z, Mark m) {
			fields[x][y][z] = m;
			r = y;
			c = x;
			h = z;
		}

	}


}
