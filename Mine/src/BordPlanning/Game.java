package BordPlanning;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

import BordPlanning.Board;

public class Game {
    
	public static final int NUMBER_PLAYERS = 2;

    private Board board;
    private Player[] players;
    private int current;

    public Game(Player s0, Player s1) {
 //       Scanner scan = new Scanner(System.in);
        
    	
    	
    	
    	
    	
    	
    	board = new Board();
        players = new Player[NUMBER_PLAYERS];
        players[0] = s0;
        players[1] = s1;
        current = 0;
    }
    
    public void start() {
        boolean continuee = true;
        while (continuee) {
            reset();
            play();
            printResult();
            continuee = readBoolean("\n> Play another time? (y/n)?", "y", "n");
        }
    }
    //Need to fix it
    private boolean readBoolean(String prompt, String yes, String no) {
        String answer;
        do {
            System.out.print(prompt);
            try (Scanner in = new Scanner(System.in)) {
                answer = in.hasNextLine() ? in.nextLine() : null;
            }
        } while (answer == null || (!answer.equals(yes) && !answer.equals(no)));
        	
        return answer.equals(yes);
    }

   
    private void reset() {
        current = 0;
        board.reset();
    }
    
    private void play() {
    	current = (int) Math.round(Math.random()); ;//random (boolean 0.0..1.0) + round (0..1)
    	while (!board.gameOver()){
    		update();
    		players[current].makeMove(board);
    		current++;
    		current = current % NUMBER_PLAYERS;
    	}
    	update();
    }
    
    private void update() {
        System.out.println("\ncurrent game situation: \n\n" + board.toString()
                + "\n");
    }
    
    private void printResult() {
        if (board.hasWinner()) {
            Player winner = board.isWinner(players[0].getMark()) ? players[0]
                    : players[1];
            System.out.println("Speler " + winner.getName() + " ("
                    + winner.getMark().toString() + ") has won!");
        } else {
            System.out.println("Draw. There is no winner!");
        }
    }
    
}
