package server;

import java.util.Scanner;

import server.Client.Level;

public class ClientTUI /*extends Observer*/ {

	public ClientTUI() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		String name = null;
		Scanner scanny = new Scanner(System.in);
		System.out.println("input name:");
		if (scanny.hasNextLine()) {
			name = scanny.nextLine();
		} 
		scanny.close();
		return name;
	}
	
	public Client.Level getLevel() {
		Level level = Level.human;
		System.out.println("Are you a human or a computer player? (h, c)");
		Scanner scanny = new Scanner(System.in);
		while (scanny.hasNextLine()) {
			String next = scanny.nextLine();
			Scanner linescanner = new Scanner(next);
			if (next.startsWith("h")) {
				level = Level.human;
				break;
			} else if (next.startsWith("c")) {
				System.out.println("Which level? (easy, medium, hard)");
				while (scanny.hasNextLine()) {
					String next2 = scanny.nextLine();
					if (next2.equals("easy")) {
						level = Level.easy;
						break;
					} else if (next2.equals("medium")) {
						scanny.nextLine();
						level = Level.medium;
						break;
					} else if (next2.equals("hard")) {
						scanny.nextLine();
						level = Level.hard;
						break;
					} else { 
						System.out.println("please input easy, medium or hard");
					}
				}
				break;
			} else {
				System.out.println("please input h for human player or c for computer player");
			}
		}
		scanny.close();
		return level;
	}
	
	public String getIP() {
		Scanner scanny = new Scanner(System.in);
		String ipaddress = null;
		System.out.println("input ip-address:");
		//TODO: make sure that the ip-address format is worked with correctly
		if (scanny.hasNextLine()) {
			ipaddress = scanny.nextLine();
		}
		return ipaddress;
	}
	
	public int getPort() {
		Scanner scanny = new Scanner(System.in);
		int port = 1234;
		System.out.println("input port:");
		if (scanny.hasNextInt()) {
			//TODO: make sure that the input is really only 4 digits.
			//TODO: if port does not exist return there is no server with this port.
			port = scanny.nextInt();
		}
		return port;
	}
	
	public void showErrorMessage(String msg) {
		System.out.println(msg);
		
	}

}
