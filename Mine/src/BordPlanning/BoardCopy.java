package BordPlanning;

public class BoardCopy {

	//------------------------------ fields ---------------------------------------------------------
	
	//the amount of columns on the board: (dimX > 0)
	public static int dimX;
	//the amount of rows on the board: (dimY > 0)
	public static int dimY;
	//the amount of floors of the board: (dimZ > 0; dimZ == -1 ? dimZ == infinity)
	public static int dimZ;
	//the amount of marks in a row needed to win a game (win < dimX || win < dimY || win < dimZ)
	public static int win;
	
	//the x coordinate of the last made move:
	public static int col;
	//the y coordinate of the last made move:
	public static int row;
	//the z coordinate of the last made move ( height):
	public static int hei;
	
	//this prints the standard measurements of the board, including x, y coordinates
	public String numbering;
	
	//the board is saved in a 3 dimensional array with values for x, y and z) Mark[z][y][x]
	private Mark[][][] fields;
	
	//--------------------------------- constructors -----------------------------------------------
	
	/*
	 * With the input of x, y and z coordinates and the win condition a new board is created
	 * if dimZ equals -1 so there are infinite floors to play on the z list in the array 
	 * should be able to have a lot of floors in there.
	 */
	public BoardCopy(int diX, int diY, int diZ, int winz) {
		dimX = diX;
		dimY = diY;
		dimZ = diZ;
		win = winz;
		
		if (dimZ == -1) {
			fields = new Mark[Integer.MAX_VALUE][dimY][dimX];
		} else {
			fields = new Mark[dimZ][dimY][dimX];
		}
		for (int z = 0; z < dimZ; z++) {
			for (int y = 0; y < dimY; y++) {
				for (int x = 0; x < dimX; x++) {
					fields[z][y][x] = Mark.EMPTY;
				}
			}
		}
	}
	
	public BoardCopy() {
		this(4, 4, 4, 4);
	}
	
	public BoardCopy(int i) {
		this(i, i, i, i);
	}
	
	//----------------------------------------- methods/queries ---------------------------------------
	
	// ~~~~~~~~~~~~~~~~~~~~~~ transfer i <-> (x,y) ~~~~~~~~~~~~~~~~~~~~~~
	/*
	 * convert an x, y coordinate into an index
	 */
	
	public int index(int x, int y) {
		return x + dimX * y;
	}
	
	/*
	 * converts an index into x coordinate
	 */
	public int iToX(int i) {
		return (i % dimX);
	}
	
	/*
	 * converts an index into a Y coordinate
	 */
	public int iToY(int i) {
		return (i - (i % dimX)) / dimX;
	}
	
	/*
	 * checks whether the inputed x and y are valid coordinates
	 */
	public boolean isField(int x, int y) {
		return 0 <= x && x < dimX && 0 <= y && y <= dimY;
	}
	
	/*
	 * gets the Mark that was put at the inputed x, y and z value
	 */
	public Mark getField(int z, int y, int x) {
		return fields[z][y][x];
	}
	
	/*
	 * checks whether the inputed x and y have a corresponding empty z value
	 * if dimZ == -1 there is always a place at a certain x and y.
	 */
	
	public boolean hasEmptyField(int x, int y) {
		if (dimZ == -1) {
			return true;
		} else {
			for (int z = 0; z < dimZ; z++) {
				if (getField(z, y, x) == Mark.EMPTY) {
					return true;
				}
			}
		}
		return false;
	}
	
	/*
	 * checks whether all the fields in the board are filled with a mark which does not 
	 * equal Mark.EMPTY.
	 */
	public boolean isFull() {
		if (dimZ == -1) {
			return false;
		} else {
			for (int z = 0; z < dimZ; z++) {
				for (int y = 0; y < dimY; y++) {
					for (int x = 0; x < dimX; x++) {
						if (getField(z, y, x) == Mark.EMPTY) {
							return false;
						}
					}
				}
			}
		}
		System.out.println("The board is full");
		return true;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~ has winner checks ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	/*
	 * checks whether the last move has made a winner in a y row
	 */
	
	public boolean hasWinY(Mark m) {
		int countY = 0;
		
		int yplus = row;
		int ymin = row - 1;
		
		while (yplus < dimY && getField(hei, yplus, col) == m) {
			countY++;
			yplus++;
		}
		while (ymin >= 0 && getField(hei, ymin, col) == m) {
			countY++;
			ymin--;
		}
		System.out.println("hasWinY (" + m.toString() + "):" + (countY >= win));
		return countY >= win;
	}
	
	/*
	 * checks whether the last move has made a winner in an x column
	 */
	
	public boolean hasWinX(Mark m) {
		int countX = 0;
		
		int xplus = col;
		int xmin = col - 1;
		
		while (xplus < dimX && getField(hei, row, xplus) == m) {
			countX++;
			xplus++;
		}
		while (xmin >= 0 && getField(hei, row, xmin) == m) {
			countX++;
			xmin--;
		}
		
		System.out.println("hasWinX (" + m.toString() + "):" + (countX >= win));
		return countX >= win;
	}
	
	/**
	 * checks whether the last move has made a winner in a z floors line
	 * @param m is the mark for which it checks whether there is a win
	 * @return boolean whether row is true
	 */
	public boolean hasWinZ(Mark m) {
		int countZ = 0;
		
		int zplus = hei;
		int zmin = hei - 1;
		
		while (zplus < dimZ && getField(zplus, row, col) == m) {
			countZ++;
			zplus++;
		}
		while (zmin >= 0 && getField(zmin, row, col) == m) {
			countZ++;
			zmin--;
		}
		System.out.println("hasWinZ (" + m.toString() + "):" + (countZ >= win));
		return countZ >= win;
	}
	
	/**
	 * checks whether there is a diagonal that is as long or longer than the win condition
	 * @param m the mark of one of the players
	 * @return boolean whether diagonal >= win
	 */
	public boolean hasDiagonalXY(Mark m) {
		//for the row (i, i, z)
		int count1 = 0;
		
		int xplus = col;
		int xmin = col - 1;
		
		int yplus = row;
		int ymin = row - 1;
		
		while (xplus < dimX && yplus < dimY && getField(hei, yplus, xplus) == m) {
			count1++;
			xplus++;
			yplus++;
		}
		while (xmin >= 0 && ymin >= 0 && getField(hei, ymin, xmin) == m) {
			count1++;
			xmin--;
			ymin--;
		}
		if (count1 >= win) {
			System.out.println("hasDiagonalXY (i, i, h) (XY) (" + m.toString() + "): true" );
			return true;
		}
		
		//for the line (i, dimY - 1 - i, z)
		int count2 = 0;
		
		int xplus2 = col;
		int xmin2 = col - 1;
		
		int yplus2 = row + 1;
		int ymin2 = row;
		
		while (xplus < dimX && ymin >= 0 && getField(hei, ymin2, xplus2) == m) {
			count2++;
			xplus++;
			ymin--;
		}
		while (xmin >= 0 && yplus < dimY && getField(hei, yplus2, xmin2) == m) {
			count2++;
			xmin++;
			yplus++;
		} if (count2 >= win) {
			System.out.println("hasDiagonalXY (i, dimY - i - 1, h) (XY) (" + m.toString() + "): true");
		} return count2 >= win;
	}
	
	public boolean hasDiagonalZX(Mark m) {
		//for (i, y i)
		int count1 = 0;
		
		int xplus = col;
		int xmin = col - 1;
		
		int zplus = hei;
		int zmin = hei - 1;
		
		while (xplus < dimX && zplus < dimZ && getField(zplus, row, xplus) == m) {
			count1++;
			xplus++;
			zplus++;
		}
		while (xmin >= 0 && zmin >= 0 && getField(zmin, row, xmin) == m) {
			count1++;
			xmin--;
			zmin--;
		}
		if (count1 >= win) {
			System.out.println("hasDiagonalXZ (i,y,i) (" + m.toString() + "): true");
			return true;
		}
		
		//for(i, y, dimZ - i - 1)
		int count2 = 0;
		
		int xplus2 = col;
		int xmin2 = col - 1;
		
		int zplus2 = hei + 1;
		int zmin2 = hei;
		
		while (xplus2 < dimX && zmin2 >= 0 && getField(zmin2, row, xplus2) == m) {
			xplus2++;
			zmin2--;
			count2++;
		}
		while (xmin2 >= 0 && zplus2 < dimZ && getField(zplus2, row, xmin2) == m) {
			xmin2--;
			zplus2++;
			count2++;
		}
		if (count2 >= win) {
			System.out.println("hasDiagonalXZ (i, y, dimZ - 1 - i) (" + m.toString() + "): true");
		}
		return count2 >= win;
	}
	
	public boolean hasDiagonalZY(Mark m) {
		//for the line (x,i,i)
		int count1 = 0;
		
		int yplus = row;
		int ymin = row - 1;
		
		int zplus = hei;
		int zmin = hei - 1;
		
		while (yplus < dimY && zplus < dimZ && getField(zplus, yplus, col) == m) {
			yplus++;
			zplus++;
			count1++;
		}
		while (ymin >= 0 && zmin >= 0 && getField(zmin, ymin, col) == m) {
			ymin--;
			zmin--;
			count1++;
		}
		if (count1 >= win) {
			System.out.println("hasDiagonalZY (x,i,i) (" + m.toString() + "): true");
			return true;
		}
		
		//for the line (x, i, dimZ - i - 1)
		int count2 = 0;
		
		int yplus2 = row;
		int ymin2 = row - 1;
		
		int zplus2 = hei + 1;
		int zmin2 = hei;
		
		while (yplus2 < dimY && zmin2 >= 0 && getField(zmin2, yplus2, col) == m) {
			yplus2++;
			zmin2--;
			count2++;
		}
		while (ymin2 >= 0 && zplus2 < dimZ && getField(zplus2, ymin2, col) == m) {
			ymin2--;
			zplus2++;
			count2++;
		}
		if (count2 >= win) {
			System.out.println("hasDiagonalZY (x, i, dimZ - i - 1) (" + m.toString() + "): true");
		}
		return count2 >= win;
	}
	
	public boolean hasDiagonalOver(Mark m) {
		//for (i,i,i)
		int count1 = 0;
		
		int xplus1 = col;
		int xmin1 = col - 1;
		
		int yplus1 = row;
		int ymin1 = row - 1;
		
		int zplus1 = hei;
		int zmin1 = hei - 1;
		
		while (xplus1 < dimX && yplus1 < dimY && zplus1 < dimZ && getField(zplus1, yplus1, xplus1) == m) {
			count1++;
			xplus1++;
			yplus1++;
			zplus1++;
		}
		while (xmin1 >= 0 && ymin1 >= 0 && zmin1 >= 0 && getField(zmin1, ymin1, xmin1) == m) {
			count1++;
			xmin1--;
			ymin1--;
			zmin1--;
		}
		if (count1 >= win) {
			System.out.println("hasDiagonalOver (i,i,i) (" + m.toString() + "): true");
			return true;
		}
		
		//for (dimX - 1 - i, i, i)
		int count2 = 0;
		
		int xplus2 = col + 1;
		int xmin2 = col;
		
		int yplus2 = row;
		int ymin2 = row - 1;
		
		int zplus2 = hei;
		int zmin2 = hei - 1;
		
		while (xmin2 >= 0 && yplus2 < dimY && zplus2 < dimZ && getField(zplus2, yplus2, xmin2) == m) {
			count2++;
			xmin2--;
			yplus2++;
			zplus2++;
		}
		while (xplus2 < dimX && ymin2 >= 0 && zmin2 >= 0 && getField(zmin2, ymin2, xplus2) == m) {
			count2++;
			xplus2++;
			ymin2--;
			zmin2--;
		}
		if (count2 >= win) {
			System.out.println("hasDiagonalOver(dimX - 1 - i, i, i) (" + m.toString() + "): true");
			return true;
		}
		
		//for (i, dimY - i - 1, i)
		int count3 = 0;
		
		int xplus3 = col;
		int xmin3 = col - 1;
		 
		int yplus3 = row + 1;
		int ymin3 = row;
		
		int zplus3 = hei;
		int zmin3 = hei - 1;
		
		while (xplus3 < dimX && ymin3 >= 0 && zplus3 < dimZ && getField(zplus3, ymin3, xplus3) == m) {
			xplus3++;
			ymin3--;
			zplus3++;
			count3++;
		}
		while (xmin3 >= 0 && yplus3 < dimY && zmin3 >= 0 && getField(zmin3, yplus3, xmin3) == m) {
			xmin3--;
			yplus3++;
			xmin3--;
			count3++;
		}
		if (count3 >= win) {
			System.out.println("hasDiagonalOver (i, dimY - 1 - i, i) (" + m.toString() + "): true");
			return true;
		}
		
		//for (i, i, dimZ - i - 1)
		int count4 = 0;
		
		int xplus4 = col;
		int xmin4 = col - 1;
		
		int yplus4 = row;
		int ymin4 = row - 1;
		
		int zplus4 = hei + 1;
		int zmin4 = hei;
		
		while (xplus4 < dimX && yplus4 < dimY && zmin4 >= 0 && getField(zmin4, yplus4, xplus4) == m) {
			xplus4++;
			yplus4++;
			zmin4--;
			count4++;
		}
		while (xmin4 >= 0 && ymin4 >= 0 && zplus4 < dimZ && getField(zplus4, ymin4, xmin4) == m) {
			xmin4--;
			ymin4--;
			zplus4++;
			count4++;
		}
		if (count4 >= win) {
			System.out.println("hasDiagonalOver (i, i, dimZ - i - 1) (" + m.toString() + "): true");
		}
		return count4 >= win;
	}
	
	/**
	 * checks for a mark whether it has won the game or not
	 * @return
	 */
	
	// !!! I actually want the game class to only check for the player who made the last move
	// whether he has made a winning move
	public boolean isWinner(Mark m) {
		return hasWinX(m) || hasWinY(m) || hasWinZ(m) || hasDiagonalXY(m) || 
				hasDiagonalZX(m) || hasDiagonalZY(m) || hasDiagonalOver(m);
	}
	
	/**
	 * checks whether a board has a winner by checking isWinner for all marks.
	 * @return
	 */
	public boolean hasWinner() {
		return isWinner(Mark.XX) || isWinner(Mark.OO);
	}
	
	/*
	 * checks whether the game is over by checking for a winner and checking whether the board is full
	 */
	public boolean gameOver() {
		return this.isFull() || this.hasWinner();
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ to string methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	public String toString() {
		String board = "";
		
		String line = "-----";
		for (int i = 0; i < dimX; i++) {
			line = line + "+---";
		}
		
		for (int z = 0; z < dimZ; z++) {
			String s = firstLine();
			for (int y = 0; y < dimY; y++) {
				String row = String.format("%-5s%s", y, "|");
				for (int x = 0; x < dimX; x++) {
					row = row + " " + getField(x, y, z).toString() + " |";
				}
				s = s + row;
				s = s + "\n" + line + "\n";
			}
			board = "Z: " + z + "\n" + s + "\n" + "\n" + board;
		}
		return board;
		
	}
	
	public String firstLine() {
		String s = "y: x: |";
		
		for (int x = 0; x < dimX; x++) {
			String space = new String();
			String space2 = new String();
			
			if (x < 10) {
				space = space2 = "  ";
			} else if (x < 100) {
				space = " ";
				space2 = "  ";
			} else {
				space = space2 = " ";
				
			}
			s = s + space + x + space2 + "|";
		}
		s = s + "\n";
		return s;
	}
	
	/**
	 * resets all the places in the board to empty
	 */
	public void reset() {
		for (int z = 0; z < dimZ; z++) {
			for (int y = 0; y < dimY; y++) {
				for (int x = 0; x < dimX; x++) {
					setField(z, y, x, Mark.EMPTY);
				}
			}
		}
	}
	
	/**
	 * you set a field by chosing an x and a y coordinate
	 * We didn't check the z as this is already done when a player tries to make a move
	 * @param x: x coordinate of move
	 * @param y: y coordinate of move
	 * @param m: the mark of the player who makes the move
	 */
	public void setField(int x, int y, Mark m) {
		if (dimZ == -1) {
			int z = 0;
			while (true) {
				if (getField(z, y, x) == Mark.EMPTY) {
					fields[z][y][x] = m;
					row = y;
					col = x;
					hei = z;
					break;
				}
				z++;
			}
		} else {
			for (int z = 0; z < dimZ; z++) {
				if (getField(z, y, x) == Mark.EMPTY) {
					fields[z][y][x] = m;
					row = y;
					col = x;
					hei = z;
					break;
				}
			}
		}
	}
	
	public void setField(int z, int y, int x, Mark m) {
		fields[z][y][x] = m;
		row = y;
		col = x;
		hei = z;
	}
	
	

}
