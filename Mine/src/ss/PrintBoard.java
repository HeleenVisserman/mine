package ss;

import java.util.Scanner;

public class PrintBoard {

	private static final String FRST = "|";
	private static final String DELIM = "    ";
	private static final String LINE =  "-----";
	private static  String linebreak ;
	private static int dim;
	private static int dim1;
	private static int zaxis;

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		dim=-1;
		while(dim!=0){
		System.out.println("Give Dimensions :");
		dim = scan.nextInt();
		System.out.println("Give Dimensions :");
		dim1= scan.nextInt();
		System.out.println("how many time to print?(Z-axis)");
		zaxis=scan.nextInt();
		for(int i=0;i<zaxis;i++){
		System.out.println(toPrint(dim,dim1) + "\n");
		 }
		}
	}

	 public static String toPrint(int num,int num2) {
	     String s = "";
	     int count=0;
		for (int i = 0; i < num; i++) {
			if(i==0){
				s= FRST +s;
			}
			linebreak = "+";
			  for (int j = 0; j < num2; j++) {
				  if(count<10){
					  	s = s+"  "+(count)+"  "+ "|";
				  }else if(count <100){
						s = s+" "+(count)+"  "+ "|";
				  }else{
						s = s+" "+(count)+" "+ "|";

				  }
					count++;
				}
			  	int var;
			  	int sum;
			  	if(dim > dim1){
			  		sum = dim-dim1;
			  	}else{
			  		sum = dim1-dim;
			  	}
				if(dim == dim1 ){
			  		var =num;
			  	}else if(dim > dim1){
			  		var =num-sum;
			  	}else {
			  		var =num+sum;
			  	}
				 for(int y=0;y<var;y++){
						linebreak = linebreak + LINE+ "+";
				  }
				  if(i == 0){
					  s = linebreak+"\n"+s +"\n"+ linebreak;
				  }else{
					  s=s +"\n"+ linebreak;
				  }
				  if(!(i== num-1)){
					  s =s + "\n"+FRST;
				  }
		}
		return s ;
	 }
}
