package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Server {

	public static String name;
	
	static List<ClientHandler> clients;
	
	public ClientHandler ch;
	public ServerSocket server;
	
	public int port;
	
	public boolean running;
	
	static final int FEATURES = 0;
	static int timeout;
	
	ServerTUI serverTUI;
	
	

	
	public static void main(String[] args) {
		new Server();
	}
	
	public Server() {
		
		serverTUI = new ServerTUI();

		Scanner scanny2 = new Scanner(System.in);
		System.out.println("input port number");
		if (scanny2.hasNextInt()) {
			port = scanny2.nextInt();
		}
		scanny2.close();
		
		
		//try to setup a new ServerSocket
		try {
			server = new ServerSocket(port);
			running = true;
			System.out.println("waiting for clients...");
		} catch (IOException e1) {
			System.out.println("serversocket could not be created");
			e1.printStackTrace();
		}
		
		//initialize the client handler and the list with clients
		ch = null;
		clients = new ArrayList<>();

		while (running) {
			//create a client handler when a client connects
			try {
				ch = new ClientHandler(server.accept(), this);
				ch.start();
				
				clients.add(ch);
			} catch (IOException e) {
				System.out.println("clienthandler could not be created");
				e.printStackTrace();
			}
			
			//when a new client connects a message is send to the server and to the other clients
			for (ClientHandler cl : clients) {
				if (cl.run && !ch.equals(cl)) {
					cl.send(name, "new client connected");
					System.out.println("A new client joined the server");
				}			
			}
		}
		
		
	}
}
